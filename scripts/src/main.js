jQuery(function ($) {

  var speed = 300;

  // debulked onresize handler
  function on_resize(c,t){onresize=function(){clearTimeout(t);t=setTimeout(c,100);};return c;}
	
  //Any input which redirects a user
  $(".js-shortcut").change(function () {
    var url = $(this).val();
    if (url !== "default") {
      window.location.href = url;
    }
  });
	
	//Toggle element visibility
  $('.js-toggle').click(function () {
    var toggleClass = $(this).data('target');
    $(this).toggleClass('toggled');
    $(target).toggle();
    return false;
  });
	
	//Toggle class
  $('.js-toggle-class').click(function () {
    var toggleClass = $(this).data('class');
    if ($(this).attr('data-target')) {
        var target = $(this).data('target');
        $(target).toggleClass(toggleClass);
        $(this).toggleClass('toggled');
    } else {
        $(this).toggleClass(toggleClass);
    }
    return false;
  });
	
	//Focus
  $('.js-focus').click(function () {
    var target = $(this).data('focus');
    var $target = $(target);
    $target.focus();
    $target.val($target.val());
  });

	//Prevent disabled links/buttons
  $('.btn, a, button, input[type=submit]').click(function (e) {
    if ($(this).hasClass('btn-disabled') || $(this).hasClass('disabled')) {
      e.preventDefault();
      return false;
    }   
  });
	
	//Tables - Responsive, activate!
  $('table').each(function () {
    var currentTable = $(this);
    if ($(this).find('th').length) {
      $(this).find('th').each(function (i) {
        var label = $(this).text();
        $(this).addClass('table-label');
        $(currentTable).find('tr').each(function () {
          $(this).find('td').eq(i).attr('data-label', label);
        });
      });
    }
  });
	
	//A variety of height based functionality which we run once up front, then again on resize
  function setHeights() {
    var eq = [
        ".js-eqh-one-of-two",
        ".js-eqh-one-of-three",
        ".js-eqh-one-of-four" //You can add your own elements if you want to (You can leave your friends behind)
        ], 
        num, 
        group_every, 
        i, 
        count, 
        max; 
        
		for (num in eq) {         
			if ($(eq[num]).length) {
				$(eq[num]).css('height', 'auto');
				group_every = 1; //Change this on a per element basis with if (eq[num] == ".featured-job") if you need to
        if(isAboveMobileSize()){
          if(eq[num] == ".js-eqh-one-of-two" || eq[num] == ".js-eqh-one-of-four" ){
            group_every = 2;
          }
        }
        if(isDesktopSize()){
          if(eq[num] == ".js-eqh-one-of-three"){
            group_every = 3;
          }
          if(eq[num] == ".js-eqh-one-of-four"){
            group_every = 4;
          }
        }
				
				i = 0;

				//Count all of our chosen
				count = $(eq[num]).size();

				//Until the current position is higher than count (ie, until we've done them all), increment by group_every.
				//This allows us to group things, for example, if you have a huge <ul> list and want each line of five to be equal, group_every 5 will achieve that without having to break them into seperate ULs.
				for (i = 0; i < count; i = i + group_every) {
					max = 0;
					$(eq[num]).slice(i, (i + group_every)).
					//Compare heights among this group
					each(function() {
            max = Math.max($(this).outerHeight(), max);
          }).height(max);
				}
        window.slimmage.CheckResponsiveImages();
			}
		}		
  }  
  on_resize(function() {
    setHeights();
  })();

  // Send scrolling events to Google Analytics http://scrolldepth.parsnip.io/
  if(typeof($.scrollDepth) == 'function') {
    $.scrollDepth();
  }
  
  //Cookies
  if (!Cookies.get('EtchCookieConsent')) {
      $('#cookie-message').addClass('cookie-message--visible');
      Cookies.set('EtchCookieConsent', true, { expires: 365 });
  }
  function isElementInViewport (el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 100 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
  }
  var isTouchDevice = 'ontouchstart' in document.documentElement;
  if (!isTouchDevice) {
      $(window).on('DOMContentLoaded load resize scroll', function(){
        //Header fade and BG move
        var $scrollTop = $(window).scrollTop();
        var heroHeight = $('.primary-header').height();

        $('.page-bg').transition({ y: $(window).scrollTop() * -0.2 }, 0);
        var multipler = 1;
        //$('.primary-header').css({
            //'opacity': 1 - ($scrollTop / (heroHeight - ($('.primary-header').height() * multipler) + 240))
        //});
        $('.primary-header').stop(true, true).transition({ y: $(window).scrollTop() * -0.2 }, 0);
        // Content rows
        $('.content-row').each(function(){
          if( isElementInViewport($(this)) ){
            $(this).addClass('content-row--animate');
          }
        });
      });
  } else {
    $('.content-row').addClass('content-row--animate');
  }
  $('.content-row').addClass('content-row--animate');

  $('.js-trigger-calendar').click(function(){
    $('.addeventatc').trigger('click');
    return false;
  });
 

}); //jQuery