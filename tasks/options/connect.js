module.exports = {
	server: {
		options: {
			base: '.',
			livereload: true,
			open: true,
      port: 9000,
			useAvailablePort: true
		}
	}
};