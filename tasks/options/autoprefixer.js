module.exports = {
	options: {
		// Task-specific options go here.
	},

	// prefix the specified file
	single_file: {
		options: {
			// Target-specific options go here.
		},
		src: '<%= paths.css.dist %>/style.css',
		dest: '<%= paths.css.dist %>/style.css'
	}
};