module.exports = {
	main: {
	    options: {
            archive: 'dist/artifact.zip'
        },
        files: [
            {
	            expand: true,
	            cwd: '.',
	            src: '<%= paths.indexFile.dist %>'
			},
			{
	            expand: true,
	            cwd: '.',
	            src: '<%= paths.coc.dist %>'
	        },
            {
            	expand: true,
	            cwd: '.',
	            src: '<%= paths.css.dist %>/*.css'
	        },
            {
            	expand: true,
	            cwd: '.',
	            src: '<%= paths.js.dist %>/*.js'
	        },
            {
            	expand: true,
	            cwd: '.',
	            src: '<%= paths.img.dist %>/*'
	        },
			{
            	expand: true,
	            cwd: '.',
	            src: '<%= paths.static.dist %>/*'
	        },
            {
            	expand: true,
            	src: 'web.config'
            }
    	]
		}
}