module.exports = {
	options: {
		algorithm: 'md5',
		length: 8
	},
	dist: {
		src: [
          '<%= paths.js.dist %>/**/*.js',
          '<%= paths.css.dist %>/**/*.css',
          '<%= paths.img.dist %>/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ]
	}
};