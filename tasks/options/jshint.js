module.exports = {
  options: {
    loopfunc: true //http://jshint.com/docs/options/#loopfunc
  },
  files: {
    src: [
      'gruntfile.js',
      '<%= paths.js.src %>/**/*.js',
      '<%= paths.js.src %>/main.js', // temporarily disable jshinting
      '!<%= paths.js.src %>/slimmage.etch.js'
    ]
  }
};