module.exports = {
	compress: {
		options: {
			report: 'gzip'
		},
		files: {
			'<%= paths.css.dist %>/style.css': ['<%= paths.css.dist %>/style.css']
		}
	},
	restructure: {
		options: {
			report: 'min'
		},
		files: {
			'<%= paths.css.dist %>/style.css': ['<%= paths.css.dist %>/style.css']
		}
	}
};