module.exports = {
	dist: {
		files: [{
			dot: true,
			src: [
				'.tmp',
				'dist/artifact.zip',
				'<%= paths.indexFile.dist %>',
				'<%= paths.js.dist %>',
				'<%= paths.css.dist %>',
				'<%= paths.img.dist %>'
			]
		}]
	},
	js: {
		files: [{
			dot: true,
			src: [
				'<%= paths.js.dist %>'
			]
		}]
	}
};