module.exports = {
	all: {
        src: '<%= paths.img.src %>/png-sprite/*.png',
        retinaSrcFilter: '<%= paths.img.src %>/png-sprite/*-2x.png',
        dest: '<%= paths.img.dist %>/sprite.png',
        retinaDest: '<%= paths.img.dist %>/sprite-2x.png',
        destCss: '<%= paths.css.src %>/_spritesmith.scss',
        imgPath: '/<%= paths.img.dist %>/sprite.png',
        retinaImgPath: '/<%= paths.img.dist %>/sprite-2x.png'
	}
};