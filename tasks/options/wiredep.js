module.exports = {
	app: {
		src: ['<%= paths.indexFile.dist %>'],
    ignorePath: /^\.\./,
		overrides: {
			"responsive": {
				"main": [
					"./build/responsive.js",
					"./build/responsive.css",
					"./build/template.html"
				]
			}
		},
		includeSelf: true
	}
};

