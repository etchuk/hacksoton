module.exports = {
    index: {
        files: [{
            dest: '<%= paths.indexFile.dist %>',
            src: '<%= paths.indexFile.src %>'
        }]
    },
    additional: {
        files: [{
            dest: '<%= paths.coc.dist %>',
            src: '<%= paths.coc.src %>'
        }]
    },
    js: {
        files: [{
            expand: true,
            cwd: '<%= paths.js.src %>/',
            dest: '<%= paths.js.dist %>/',
            src: '**/*.js'
        }]
    },
    img: {
        files: [{
            expand: true,
            cwd: '<%= paths.img.src %>/',
            dest: '<%= paths.img.dist %>/',
            src: ['*.png', '*.gif', '*.jpg', '*.jpeg', '*.svg']
        }]        
    }
};