module.exports = {
	dynamic: {
		files: [{
			expand: true,
			cwd: '<%= paths.img.dist %>/',
			src: ['<%= paths.img.src %>/**/*.{png,jpg,gif}'],
			dest: '<%= paths.img.dist %>/'
		}]
	}
};