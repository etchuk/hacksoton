// Performs rewrites based on the useminPrepare configuration
module.exports = {
	html: ['<%= paths.indexFile.dist %>', '<%= paths.coc.dist %>'],
	css: ['<%= paths.css.dist %>/*.css'],
	js: ['<%= paths.js.dist %>/*.js'],
	options: {
	    assetsDirs: ['.'],
	    patterns: {
           js: [
                 [/(img\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
           ],
           css: [
                 [/(img\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the CSS to reference our revved images']
           ]
       }
	}
};