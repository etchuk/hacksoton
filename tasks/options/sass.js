module.exports = {
	options: {
		sourceMap: true
	},
	dist: {
		files: {
			'<%= paths.css.dist %>/style.css': '<%= paths.css.src %>/style.scss'
		}
	}
};