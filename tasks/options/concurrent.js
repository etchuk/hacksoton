module.exports = {
	serveAndWatch: {
		tasks: ['serve', 'watch'],
		options: {
			logConcurrentOutput: true
		}
	}
};
