module.exports = {
	options: {
		livereload: true,
	},
	bower: {
		files: ['bower.json'],
		tasks: ['wiredep']
	},
	scripts: {
        files: ['<%= paths.js.src %>/**/*.js'],
        tasks: [/*'jshint',*/ 'copy:js']
    },
    index: {
        files: ['<%= paths.indexFile.src %>'],
        tasks: ['copy:index', 'wiredep']	
    },
	css: {
		files: ['<%= paths.css.src %>/**/*.scss'],
		tasks: ['sass', 'autoprefixer']
	},
	img: [{
        files: ['<%= paths.img.src %>/*.png'],
        tasks: ['copy:img']
	}]
};