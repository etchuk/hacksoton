module.exports = function(grunt) {

	var config = {
		pkg: grunt.file.readJSON('package.json'),
		paths: {
			indexFile: {
				src: '_index.html',
				dist: 'index.html'
			},
			coc: {
				src: 'code-of-conduct.html',
				dist: 'code-of-conduct.html'
			},
			css: {
				src: 'css/src',
				dist: 'css/dist'
			},
			js: {
				src: 'scripts/src',
				dist: 'scripts/dist'
			},
			img: {
				src: 'img/src',
				dist: 'img/dist'
			},
			static: {
				dist: 'img/static'
			}
		},
		env: process.env
	};

	// http://www.thomasboyt.com/2013/09/01/maintainable-grunt.html
	require('load-grunt-tasks')(grunt);

	// load any custom tasks defined in the /tasks folder
	grunt.loadTasks('tasks');
	// load config for all out tasks
	grunt.util._.extend(config, loadConfig('./tasks/options/'));

    grunt.initConfig(config);


    // single-run regular development build
	grunt.registerTask('build', [
		'bower:install',
		'jshint',
    	//'csslint',
		'clean:dist',
		'imagemin',
		'sprite',
		'sass',
		'autoprefixer',
		'copy:js',
		'copy:index',
		'copy:img',
		'wiredep'
	]);

    // single-run minified/concatentated production build
	grunt.registerTask('package', [
		'build',
		'clean:js', // clear out the individual dist js files for a production build
		//'csso',
		'useminPrepare',
	    'concat:generated',
	    'cssmin:generated',
	    'uglify:generated',
	    'filerev',
		'usemin',
		'compress'
	]);

	// continuous watch development build
	grunt.registerTask('default', [
		'build',
		'watch'
	]);

	// continuous watch development build with static file server
	grunt.registerTask('serve', [
		'build',
		'connect',
		'watch'
	]);


	function loadConfig(path) {
		var glob = require('glob');
		var object = {};
		var key;

		glob.sync('*', {cwd: path}).forEach(function(option) {
			key = option.replace(/\.js$/, '');
			object[key] =require(path + option);
		});

		return object;
	}

};